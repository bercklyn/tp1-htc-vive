using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (Camera))]

public class RangeFinder : MonoBehaviour 
{
	//==============================================================
	// Variable for the Sniper Camera component
	//==============================================================
	[Header("Sniper Camera")]
	public Camera sniperCam;

	public static int zoomFactor;

	private Text osdText;
	private Vector3 startPoint;
	private float distance;
	private float timeleft = 0.0f;	// Left time for current update interval
	private float updateInterval = 0.25f;

	public enum unit 
	{
		meter, yards
	}
	public unit RangeUnit;

	//==============================================================
	// At Start, get the Camera component and position
	//==============================================================
	void Start()
	{
		osdText = GetComponent<Text>();
		startPoint = sniperCam.transform.position;
		timeleft = updateInterval;
	}

	void Update() 
	{
		timeleft -= Time.deltaTime;
		RaycastHit hit;
		//==============================================================
		// Interval ended - update GUI text and start new interval
		//==============================================================
		if( timeleft <= 0.0 )
		{
			if (Physics.Raycast (Camera.main.ScreenPointToRay (new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f)), out hit)) 
			{
				distance = Vector3.Distance (startPoint, hit.point);

				if (RangeUnit == 0)  
				{
					osdText.text = "RNG " + (distance).ToString ("f2") + " m" + "\n\n" + "ZOOM x" + zoomFactor;
				}
				else
				{
					osdText.text = "RNG " + (distance*1.0936133).ToString ("f2") + " yd" + "\n\n" + "ZOOM x" + zoomFactor;
				} 
				startPoint = sniperCam.transform.position;
			} 
			else 
			{
				osdText.text = "RNG INF" + "\n\n" + "ZOOM x" + zoomFactor;
			}
			timeleft = updateInterval;
		}
	}
}
