﻿using UnityEngine;
using System.Collections;

public class MouseLookOrbit : MonoBehaviour
{
	private Transform MyCam;
	private float xDeg = 0.0f;
	private float yDeg = 2.5f;
	private Quaternion currentRotation;
	private Quaternion desiredRotation;
	//private Quaternion startingRotation;

	public int yMinLimit = -60;
	public int yMaxLimit = 60;
	public static float xSpeed = 200.0f;
	public static float ySpeed = 200.0f;
	public float zoomDampening = 10.0f;

	private Quaternion rotation;

	void Start()
	{
		MyCam = transform;
	}

	void LateUpdate ()
	{
		FreeLook ();
		if (Input.GetKey(KeyCode.Escape))	// Show Mousecursor
		{
			Cursor.visible = true;
		}
	}

    private void FreeLook()
    {
        // Orbiting camera
        xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
        yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;


         var camare = GameObject.Find("MainCamera");

         var control   = GameObject.Find("Controller (right)");

        //var canvas = GameObject.Find("CanvasSniper");
        //Clamp the vertical axis for the orbit
        yDeg = ClampAngle(yDeg, yMinLimit, yMaxLimit);

        //   set camera rotation
        //desiredRotation = Quaternion.Euler(yDeg, xDeg, 0);
        //currentRotation = MyCam.localRotation;
        //rotation = Quaternion.Lerp(currentRotation, desiredRotation, Time.deltaTime * zoomDampening);
        //MyCam.localRotation = rotation;

        MyCam.localRotation = camare.transform.localRotation;

        //canvas.transform.SetPositionAndRotation(control.transform.position,control.transform.rotation);
        //camare.transform.rotation = Quaternion.LookRotation(control.transform.position - camare.transform.position);
        // MyCam.localRotation = Quaternion.LookRotation(control.transform.position - MyCam.position);
    }

	private static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;
		return Mathf.Clamp (angle, min, max);
	}
}