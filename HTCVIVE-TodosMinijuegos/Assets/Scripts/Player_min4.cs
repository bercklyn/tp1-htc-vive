﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player_min4 : MonoBehaviour {

    public int HitPoints;
    private bool Win;
    private void Start()
    {
        Win = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Zombie"))
        {
            Destroy(collision.gameObject);
            HitPoints--;

            if (HitPoints == 0)
            {
                GameOver("GAME OVER");
            }

        }
    }
    private void FixedUpdate()
    {
        if (Win)
        {
            GameOver("¡YOU WIN!");
        }
    }

    private void GameOver(string mensaje)
    {
        // logica cuando acaba el juego
        var textTitle = GameObject.Find("MiniJuego3").transform.Find("TextTitle").gameObject;
        var count = GameObject.Find("MiniJuego3").transform.Find("TextCount").gameObject;
        //GameObject.Find("SpawnerEnemy").gameObject.GetComponent<SpawnerEnemy>().ResetSpawner();
        //GameObject.Find("SpawnerEnemy").gameObject.GetComponent<SpawnerEnemy>().enabled = false;
        textTitle.GetComponent<TextMeshPro>().text = mensaje;
        count.GetComponent<ScriptCountToHome>().StartCount(2);
    }
    public void ResetPlayer()
    {
        HitPoints = 2;
    }

    public void win()
    {
        Win = true;
    }
}
