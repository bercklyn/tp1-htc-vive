﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego4 : MonoBehaviour {

    public float slow;
    public Transform cameraRigTransform;
    public Transform headTransform; // The camera rig's head
    public LayerMask teleportMask; // Mask to filter out areas where teleports are allowed
    public GameObject SpawnerZombie;
    public GameObject laserPrefab; // The laser prefab

    public GameObject ControlLeft;
    public GameObject ControlRight;
    public GameObject CanvasCamera;
    public GameObject LightCameraMain;
    public bool isPaused;
    public bool isEndGame;


    private GameObject laser; // A reference to the spawned laser
    private Transform laserTransform; // The transform component of the laser for ease of use

    private Transform teleportReticleTransform; // Stores a reference to the teleport reticle transform for ease of use

    private Vector3 hitPoint; // Point where the raycast hits
    private bool shouldTeleport; // True if there's a valid teleport target
    private GameObject raycastObject;


    private SteamVR_TrackedObject trackedObj;
    private int contZoom;
    private int secondChangeZoom;
    private int xZoom;
    private float[] zoomFov = { 1, 4, 8, 15, 30, 60 }; // { 60, 30, 15, 8, 4, 1 };
    private float timeScale;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        secondChangeZoom = 2;
        xZoom = 5;
        //timeScale = 6;
    }
    

    void Update()
    {
        //Time.timeScale = slow;
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if (!isEndGame)
                PauseGame();
        }
        if (isPaused || isEndGame)
        {
            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
            {
                RaycastHit hit;

                // Send out a raycast from the controller
                if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
                {
                    hitPoint = hit.point;
                    raycastObject = hit.collider.gameObject;

                    ShowLaser(hit);

                    shouldTeleport = true;
                }
            }
            else // Touchpad not held down, hide laser & teleport reticle
            {
                laser.SetActive(false);
            }

            // Touchpad released this frame & valid teleport position found
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
            {
                //logica de Teleport segun juego
                switch (raycastObject.name)
                {
                    case "Nova_Salir": //va al inicio    
                        Time.timeScale = 1;
                        SceneManager.LoadScene("Game");
                        break;
                    case "Nova_Again": // vuelves a jugar
                        ControlLeft.GetComponent<ControlJuego4>().isEndGame = false;
                        ControlRight.GetComponent<ControlJuego4>().isEndGame = false;
                        SpawnerZombie.GetComponent<SpawnerTroop>().ResetGame();
                        break;
                    case "Nova_Control":
                        var panelinst = CanvasCamera.transform.Find("Nova_Pause").Find("PanelInstruccion");
                        panelinst.transform.Find("UsoMando").gameObject.SetActive(true);
                        panelinst.transform.Find("Informacion").gameObject.SetActive(false);
                        break;
                    case "Nova_Informacion":
                        var panelinst2 = CanvasCamera.transform.Find("Nova_Pause").Find("PanelInstruccion");
                        panelinst2.transform.Find("UsoMando").gameObject.SetActive(false);
                        panelinst2.transform.Find("Informacion").gameObject.SetActive(true);
                        break;
                    case "Salir":
                        PauseGame();
                        break;
                }
            }

        }
        else
        {
            if (Controller.GetAxis().x != 0 && contZoom<=0)
            {
                Zoom();
            }

            // cuando se presiona el gatillo ocurre esto
            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Trigger))
            {
                if (name.Contains("right"))
                {
                    Shoot();
                }
            }
            if (Controller.GetHairTriggerDown())
            {
                if (name.Contains("left"))
                {
                    Reload();
                }
            }
            contZoom--;

        }



    }

    private void Shoot()
    {
        var DSR50 = GameObject.Find("SniperDSR50");
        DSR50.GetComponent<DSR50>().PlayShoot();
    }

    private void Reload()
    {
        var DSR50 = GameObject.Find("SniperDSR50");
        DSR50.GetComponent<DSR50>().ReloadMachineGun();
    }

    private void Zoom()
    {
        var input = Controller.GetAxis().x;
        contZoom = secondChangeZoom;
        var sniperCamera = GameObject.Find("ScopeCamera");
        var scopeCamera = sniperCamera.GetComponent<Camera>();
        

        if(input > 0)
        {
            if (xZoom > 0)
            {
                xZoom--;
                scopeCamera.fieldOfView = zoomFov[xZoom];
            }
        }
        else
        {
            if (xZoom < zoomFov.Length)
            {
                xZoom++;
                scopeCamera.fieldOfView = zoomFov[xZoom];

            }
        }

    }

    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        isPaused = false;
    }

    private void ShowLaser(RaycastHit hit)
    {
        laser.SetActive(true); //Show the laser
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f); // Move laser to the middle between the controller and the position the raycast hit
        laserTransform.LookAt(hitPoint); // Rotate laser facing the hit point
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance); // Scale laser so it fits exactly between the controller & the hit point
    }

    private void ShowPause(bool show)
    {
        CanvasCamera.transform.Find("Nova_Pause").gameObject.SetActive(show);
        CanvasCamera.transform.Find("Panel").gameObject.SetActive(show);
        ControlRight.transform.Find("SniperDSR50").gameObject.SetActive(!show);
        LightCameraMain.SetActive(show);

    }

    private void PauseGame()
    {
        if (!isPaused)
        {
            Time.timeScale = 0;
            isPaused = true;
            ShowPause(true);

        }
        else
        {
            Time.timeScale = 1;
            isPaused = false;
            ShowPause(false);

        }
    }
}
