﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainHome : MonoBehaviour {

    public Transform cameraRigTransform;
    public Transform headTransform; // The camera rig's head
    public Vector3 teleportReticleOffset; // Offset from the floor for the reticle to avoid z-fighting
    public LayerMask teleportMask; // Mask to filter out areas where teleports are allowed
    public LayerMask teleportMaskMain; // Mask to filter
    public GameObject canvasCamera;
    public GameObject controlLeft;
    public GameObject controlRight;
    public GameObject NovaOpciones;
    public GameObject NovaPizarra;



    private SteamVR_TrackedObject trackedObj;

    public GameObject laserPrefab; // The laser prefab
    public GameObject laserPrefabMain; // The laser prefab

    private GameObject laser; // A reference to the spawned laser
    private Transform laserTransform; // The transform component of the laser for ease of use
    private GameObject laser2; // A reference to the spawned laser
    private Transform laserTransform2; // The transform component of the laser for ease of use

    public GameObject teleportReticlePrefab; // Stores a reference to the teleport reticle prefab.
    private GameObject reticle; // A reference to an instance of the reticle
    private Transform teleportReticleTransform; // Stores a reference to the teleport reticle transform for ease of use

    private Vector3 hitPoint; // Point where the raycast hits
    private bool shouldTeleport; // True if there's a valid teleport target
    private GameObject raycastObject;
    private bool Paused;
    private bool viewInformation;
    private int numGameSelect;
    

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    //new
    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        laser2 = Instantiate(laserPrefabMain);
        laserTransform2 = laser2.transform;
        reticle = Instantiate(teleportReticlePrefab);
        teleportReticleTransform = reticle.transform;
        Paused = false;
        viewInformation = false;
    }

    void Update()
    {
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if(!viewInformation)
                PauseGame();
        }


        // Is the touchpad held down?
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            RaycastHit hit;

            // Send out a raycast from the controller
            if (!Paused)
            {
                if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
                {
                    hitPoint = hit.point;
                    raycastObject = hit.collider.gameObject;

                    ShowLaser(hit,false);

                    //Show teleport reticle
                    reticle.SetActive(true);
                    teleportReticleTransform.position = hitPoint + teleportReticleOffset;

                    shouldTeleport = true;
                }

            }
            else
            {
                if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMaskMain))
                {
                    hitPoint = hit.point;
                    raycastObject = hit.collider.gameObject;

                    ShowLaser(hit,true);

                    shouldTeleport = true;
                }

            }

        }
        else // Touchpad not held down, hide laser & teleport reticle
        {
            laser.SetActive(false);
            laser2.SetActive(false);

            reticle.SetActive(false);
        }

        // Touchpad released this frame & valid teleport position found
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
        {
            if(!Paused)
            {
                switch (raycastObject.name)
                {
                    case "TunnelJuego1":

                        SceneManager.LoadScene("MiniJuego1");

                        break;
                    case "TunnelJuego2":

                        SceneManager.LoadScene("MiniJuego2");


                        break;
                    case "TunnelJuego3":

                        SceneManager.LoadScene("MiniJuego3");


                        break;

                    case "TunnelJuego4":

                        SceneManager.LoadScene("MiniJuego4");

                        break;

                }
                Teleport();
                return;
            }


            //logica de Teleport segun juego
            switch (raycastObject.name)
            {
                case "Mando":
                    NovaPizarra.transform.Find("Instrucciones").gameObject.SetActive(false);
                    NovaPizarra.transform.Find("Mando").gameObject.SetActive(true);
                    NovaPizarra.transform.Find("AcercaDe").gameObject.SetActive(false);

                    break;
                case "AcercaDe":
                    NovaPizarra.transform.Find("Instrucciones").gameObject.SetActive(false);
                    NovaPizarra.transform.Find("Mando").gameObject.SetActive(false);
                    NovaPizarra.transform.Find("AcercaDe").gameObject.SetActive(true);

                    break;
                case "Instrucciones":
                    NovaPizarra.transform.Find("Instrucciones").gameObject.SetActive(true);
                    NovaPizarra.transform.Find("Mando").gameObject.SetActive(false);
                    NovaPizarra.transform.Find("AcercaDe").gameObject.SetActive(false);

                    break;
                case "TunnelJuego1":

                    SceneManager.LoadScene("MiniJuego1");

                    break;
                case "TunnelJuego2":

                    SceneManager.LoadScene("MiniJuego2");


                    break;
                case "TunnelJuego3":

                    SceneManager.LoadScene("MiniJuego3");

                    
                    break;

                case "TunnelJuego4":

                    SceneManager.LoadScene("MiniJuego4");

                    break;

            }
        }
    }

    private void ShowLaser(RaycastHit hit,bool main)
    {
        if (main)
        {
            laser2.SetActive(true); //Show the laser
            laserTransform2.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f); // Move laser to the middle between the controller and the position the raycast hit
            laserTransform2.LookAt(hitPoint); // Rotate laser facing the hit point
            laserTransform2.localScale = new Vector3(laserTransform2.localScale.x, laserTransform2.localScale.y,
                hit.distance); 
        }
        else
        {
            laser.SetActive(true); //Show the laser
            laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f); // Move laser to the middle between the controller and the position the raycast hit
            laserTransform.LookAt(hitPoint); // Rotate laser facing the hit point
            laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
                hit.distance);
        }

        // Scale laser so it fits exactly between the controller & the hit point
    }

    private void Teleport()
    {
        shouldTeleport = false; // Teleport in progress, no need to do it again until the next touchpad release
        reticle.SetActive(false); // Hide reticle
        Vector3 difference = cameraRigTransform.position - headTransform.position; // Calculate the difference between the center of the virtual room & the player's head
        difference.y = 0; // Don't change the final position's y position, it should always be equal to that of the hit point

        cameraRigTransform.position = hitPoint + difference + new Vector3(0, 0.25f, 0); // Change the camera rig position to where the the teleport reticle was. Also add the difference so the new virtual room position is relative to the player position, allowing the player's new position to be exactly where they pointed. (see illustration)
    }

    private void PauseGame()
    {
        if (!Paused)
        {
            //Time.timeScale = 0;
            Paused = true;
            canvasCamera.SetActive(true);
        }
        else
        {
           // Time.timeScale = 1;
            Paused = false;
            canvasCamera.SetActive(false);
        }
    }
    
}
