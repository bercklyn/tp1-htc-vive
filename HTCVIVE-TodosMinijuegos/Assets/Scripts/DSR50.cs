﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DSR50 : MonoBehaviour {

    public GameObject prefabBullet;
    public int force;
    public int Cartucho;
    public float contSecondLoad;
    public int contSecondReLoad;

    private int ContBullets;
    private float contSecond;

    //private AudioSource source;

    // Use this for initialization
    void Start()
    {
        contSecond = contSecondLoad;
        ContBullets = Cartucho;
       // source = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        var rigi = gameObject.GetComponent<Rigidbody>();
        Vector3 velocity = rigi.velocity * TimerManager.getnstance().mytimeScale;
        rigi.velocity = velocity;
        ///transform.rotation = new Quaternion(transform.rotation.x/ 2,transform.rotation.y/2, transform.rotation.z/2,transform.rotation.w/2);
        contSecond--;
    }

    public void PlayShoot()
    {
        if (ContBullets > 0 && contSecond <= 0)
        {
            var positionShoot = transform.Find("Shoot").transform;
            var bullet = Instantiate(prefabBullet, positionShoot.position, transform.rotation);

            // sonido de disparo
           // source.Play();
            bullet.name = "Bullet";
            var rigibody = bullet.GetComponent<Rigidbody>();
            // rigibody.useGravity = true;
            rigibody.velocity = positionShoot.forward * force;
            contSecond = contSecondLoad * 60;
            ContBullets--;
            // se puede añadir la animacin de disparo miniexplosion del arma
        }

    }

    public void ReloadMachineGun()
    {
        ContBullets = Cartucho;
        contSecond = contSecondReLoad * 60;
    }
}
