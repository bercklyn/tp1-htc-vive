﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTres : MonoBehaviour {
    public int lifeBarMax;
    private int life;
    // Use this for initialization
    void Start()
    {
        life = lifeBarMax;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("zombie"))
        {
            Destroy(collision.gameObject);
            life--;
            if (life == 0)
            {
                // acaba el juego   cuando  pierdes  todos  tus puntos de vida
                var game3 = GameObject.Find("MiniJuego3").transform;
                game3.Find("SpawnerZombie").GetComponent<SpawnerZombie>().GameOver(0);
                ResetLife();
            }
        }
    }

    public void ResetLife()
    {
        life = lifeBarMax;
    }
}
