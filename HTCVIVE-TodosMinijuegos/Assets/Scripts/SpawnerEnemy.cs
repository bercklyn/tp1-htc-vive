﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemy : MonoBehaviour {

    public GameObject prefabEnemy;
    public GameObject controlLeft;
    public GameObject controlRight;
    public GameObject Player;
    

    public GameObject PositionPlayer;
    public GameObject PositionSpawner2;
    public Material EnemySlow;
    public Material EnemyFast;
    public Material EnemyMediun;

    private float time;
    private float spawnTime;
    private Enemy scriptEnemy;
    private bool isPlaying;
    // Use this for initialization


    void Start () {

        time = 7;
        spawnTime = 12;
        isPlaying = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (isPlaying)
        {
            time += Time.deltaTime;

            if (time >= spawnTime)
            {
                NewEnemy();
                SetSpawnTimeRandom();
            }
        }

    }
    
    private void NewEnemy()
    {
        time = 0;
        var random = Random.Range(1,3);

        if (random == 1)
             scriptEnemy = Instantiate(prefabEnemy, transform.position, transform.rotation).GetComponent<Enemy>();

        else
             scriptEnemy = Instantiate(prefabEnemy, PositionSpawner2.transform.position, PositionSpawner2.transform.rotation).GetComponent<Enemy>();

        scriptEnemy.SetAtributeEnemy(EnemySlow, EnemyMediun, EnemyFast, PositionPlayer);

    }

    void SetSpawnTimeRandom()
    {
        spawnTime = Random.Range(7,10);
    }
    public void ResetSpawner()
    {
        time = 7;
        spawnTime = 12;
    }

    public  void GameOver()
    {
        isPlaying = false;
        controlLeft.transform.GetChild(0).gameObject.SetActive(false);
        controlRight.transform.GetChild(0).gameObject.SetActive(false);

        controlLeft.transform.GetComponent<ControlJuego2>().isEndGame = true;
        controlRight.transform.GetComponent<ControlJuego2>().isEndGame = true;
    }

    public void ResetGame()
    {
        isPlaying = true;
        controlLeft.transform.GetChild(0).gameObject.SetActive(true);
        controlRight.transform.GetChild(0).gameObject.SetActive(true);
        Player.GetComponent<Player>().ResetPlayer();
    }
}
