﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : MonoBehaviour {
    public float myDelta;
    public float myFixeDelta;
    public float mytimeScale = 1;
    public static TimerManager instance;
    public static TimerManager getnstance()
    {
        return instance;
    }


    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        myDelta = Time.deltaTime * mytimeScale;
	}

    private void FixedUpdate()
    {
        myFixeDelta = Time.fixedDeltaTime * mytimeScale;
    }
}
