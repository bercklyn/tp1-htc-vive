﻿
using UnityEngine;
using UnityEngine.SceneManagement;
public class LaserPointer : MonoBehaviour
{
    public Transform cameraRigTransform;
    public Transform headTransform; // The camera rig's head
    public Vector3 teleportReticleOffset; // Offset from the floor for the reticle to avoid z-fighting
    public LayerMask teleportMask; // Mask to filter out areas where teleports are allowed

    private SteamVR_TrackedObject trackedObj;

    public GameObject laserPrefab; // The laser prefab
    private GameObject laser; // A reference to the spawned laser
    private Transform laserTransform; // The transform component of the laser for ease of use

    public GameObject teleportReticlePrefab; // Stores a reference to the teleport reticle prefab.
    private GameObject reticle; // A reference to an instance of the reticle
    private Transform teleportReticleTransform; // Stores a reference to the teleport reticle transform for ease of use

    private Vector3 hitPoint; // Point where the raycast hits
    private bool shouldTeleport; // True if there's a valid teleport target
    private GameObject raycastObject;
    private Game gameScript;
    private bool Paused;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    //new
    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        reticle = Instantiate(teleportReticlePrefab);
        teleportReticleTransform = reticle.transform;
        gameScript = GameObject.Find("Game").GetComponent<Game>();
    }

    void Update()
    {
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if (!Paused)
            {
                Time.timeScale = 0;
                Paused = true;
            }
            else
            {
                Time.timeScale = 1;
                Paused = false;
            }
        }


        // Is the touchpad held down?
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            RaycastHit hit;

            // Send out a raycast from the controller
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
            {
                hitPoint = hit.point;
                raycastObject = hit.collider.gameObject;

                ShowLaser(hit);

                //Show teleport reticle
                reticle.SetActive(true);
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;

                shouldTeleport = true;
            }
        }
        else // Touchpad not held down, hide laser & teleport reticle
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }

        // Touchpad released this frame & valid teleport position found
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
        {
            Teleport();

            //logica de Teleport segun juego
            switch (raycastObject.name)
            {
                case "TunnelJuego1":

                    SceneManager.LoadScene("MiniJuego1");
                    
                    ////logica para inicializar el primner juego
                    ////var escudoTransform = GameObject.Find("Escudo").GetComponent<Transform>();
                    //var minijuego1 = gameScript.GetMinijuego1(); 
                    ////activamos el escenario del minijuego
                    //minijuego1.SetActive(true);

                    //var positionGame = minijuego1.transform.FindChild("PositionInitial").transform;
                    //var startBoss = GameObject.Find("MiniJuego1").transform.FindChild("PositionBoss").transform;
                    //var boss = GameObject.Find("Boss");
                    //var camareTrasform = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                    ////var positionPlayer = camareTrasform.Find("PositionPlayer");



                    ////positionPlayer.GetComponent<CapsuleCollider>().enabled = false;

                    //camareTrasform.SetPositionAndRotation(positionGame.position, camareTrasform.rotation);
                    //startBoss.GetComponent<StartBoss>().StartMiniGame();

                    ////boss.GetComponent<Spawner>().enabled=true;

                    break;
                case "TunnelJuego2":

                    //SceneManager.LoadScene("MiniJuego2");

                    ////logica para inicializar el segundo juego
                    //var minijuego2 = gameScript.GetMinijuego2();
                    ////activamos el escenario del minijuego
                    //minijuego2.SetActive(true);

                    //var positionGame2 = minijuego2.transform.FindChild("PositionInitial").gameObject;

                    //var controlLeft = GameObject.Find("Controller (left)");
                    //var controlRight = GameObject.Find("Controller (right)");
                    //var camareTrasform2 = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                    //var positionPlayer = camareTrasform2.Find("PositionPlayer");
                    //var lightcamare = camareTrasform2.Find("LightCamare");



                    ////  apagamos la luz da camara 
                    //lightcamare.GetComponent<Light>().intensity = 0.35f;

                    //// Activamos el script del jugador , su capsule collider y agregamos  un rigibody al player
                    //positionPlayer.GetComponent<Player>().enabled = true;
                    //positionPlayer.GetComponent<CapsuleCollider>().enabled = true;


                    //// desabilitamos el box collider y el laserpointer
                    //controlLeft.GetComponent<BoxCollider>().enabled = false;
                    //controlRight.GetComponent<BoxCollider>().enabled = false;

                    ////controlLeft.GetComponent<LaserPointer>().enabled = false;
                    ////controlRight.GetComponent<LaserPointer>().enabled = false;

                    //// destruimos el rigibody

                    //if (controlLeft.GetComponent<Rigidbody>() != null)
                    //    Destroy(controlLeft.GetComponent<Rigidbody>());

                    //if (controlRight.GetComponent<Rigidbody>() != null)
                    //    Destroy(controlRight.GetComponent<Rigidbody>());

                    //// desactivamos  los models  de los controles
                    //controlLeft.transform.FindChild("Model").gameObject.SetActive(false);
                    //controlRight.transform.FindChild("Model").gameObject.SetActive(false);

                    //// activa el arco del control y spawnerenemy
                    //controlLeft.transform.FindChild("Arco").gameObject.SetActive(true);
                    //GameObject.Find("SpawnerEnemy").gameObject.GetComponent<SpawnerEnemy>().enabled = true;
                    ////GameObject.Find("SpawnerEnemy2").gameObject.GetComponent<SpawnerEnemy>().enabled = true;

                    //// activamos el script de control right
                    //controlRight.GetComponent<ArrowManager>().enabled = true;


                    //var rigibody = positionPlayer.gameObject.AddComponent<Rigidbody>();
                    //rigibody.useGravity = false;
                    //rigibody.constraints = RigidbodyConstraints.FreezeAll;

                    ////  seteamos finalmente  la posicion del  jugador
                    //camareTrasform2.SetPositionAndRotation(positionGame2.transform.position, camareTrasform2.rotation);

                    break;
                case "TunnelJuego3":

                    SceneManager.LoadScene("MiniJuego3");


                    //var minijuego3 = gameScript.GetMinijuego3();
                    //// activamos el escenario del minijuego
                    //minijuego3.SetActive(true);

                    //var positionGame3 = GameObject.Find("MiniJuego3").transform.FindChild("PositionInitial").gameObject;

                    //var controlLeft3 = GameObject.Find("Controller (left)");
                    //var controlRight3 = GameObject.Find("Controller (right)");
                    //var camareTrasform3 = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                    //var positionPlayer3 = camareTrasform3.Find("PositionPlayer");
                    //var lightcamare2 = camareTrasform3.Find("LightCamare");


                    ////activamos el collider 
                    //camareTrasform3.Find("Camera (eye)").Find("PositionCamera").gameObject.SetActive(true);

                    ////  apagamos la luz da camara 
                    //lightcamare2.gameObject.SetActive(false);

                    //// desactivamos  los models  de los controles
                    //controlLeft3.transform.Find("Model").gameObject.SetActive(false);
                    //controlRight3.transform.Find("Model").gameObject.SetActive(false);

                    ////desactivamos los scripts del mando
                    //controlLeft3.GetComponent<ControllerGrabObject>().enabled = false;
                    //controlRight3.GetComponent<ControllerGrabObject>().enabled = false;


                    //// activamos el script de la metralladora
                    //controlLeft3.GetComponent<ControlJuego3>().enabled = true;
                    //controlRight3.GetComponent<ControlJuego3>().enabled = true;


                    //// instaciamos  el model de la metralladora

                    //controlRight3.transform.Find("AK47").gameObject.SetActive(true);

                    //// inicializamos el spaqner de los zombies
                    //var spawner = minijuego3.transform.Find("SpawnerZombie");
                    //spawner.GetComponent<SpawnerZombie>().PlayGame();

                    //// seteamos  la posicion de la camara del jugador 
                    //camareTrasform3.SetPositionAndRotation(positionGame3.transform.position, camareTrasform3.rotation);
                    break;

                case "TunnelJuego4":

                    //SceneManager.LoadScene("MiniJuego4");


                    //var minijuego4 = gameScript.GetMinijuego4();
                    ////activamos el escenario del minijuego
                    //minijuego4.SetActive(true);

                    //var positionGame4 = minijuego4.transform.FindChild("PositionInitial").gameObject;

                    //var controlLeft4 = GameObject.Find("Controller (left)");
                    //var controlRight4 = GameObject.Find("Controller (right)");
                    //var camareTrasform4 = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                    //var positionPlayer4 = camareTrasform4.Find("PositionPlayer");
                    ////var lightcamare4 = camareTrasform4.Find("LightCamare");



                    //// desactivamos  los models  de los controles
                    //controlLeft4.transform.Find("Model").gameObject.SetActive(false);
                    //controlRight4.transform.Find("Model").gameObject.SetActive(false);

                    ////desactivamos los scripts del mando
                    //controlLeft4.GetComponent<ControllerGrabObject>().enabled = false;
                    //controlRight4.GetComponent<ControllerGrabObject>().enabled = false;


                    //// activamos el script del sniper
                    //controlLeft4.GetComponent<ControlJuego4>().enabled = true;
                    //controlRight4.GetComponent<ControlJuego4>().enabled = true;

                    ////activamos el arma sniper DSR50
                    //controlRight4.transform.Find("SniperDSR50").gameObject.SetActive(true);

                    ////iniciamos el juego
                    //var SpawnerTroop = minijuego4.transform.Find("SpawnTroop").GetComponent<SpawnerTroop>();
                    //SpawnerTroop.PlayGame();


                    //camareTrasform4.SetPositionAndRotation(positionGame4.transform.position, positionGame4.transform.rotation);
                    
                    break;

            }
        }
    }

    private void ShowLaser(RaycastHit hit)
    {
        laser.SetActive(true); //Show the laser
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f); // Move laser to the middle between the controller and the position the raycast hit
        laserTransform.LookAt(hitPoint); // Rotate laser facing the hit point
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance); // Scale laser so it fits exactly between the controller & the hit point
    }

    private void Teleport()
    {
        shouldTeleport = false; // Teleport in progress, no need to do it again until the next touchpad release
        reticle.SetActive(false); // Hide reticle
        Vector3 difference = cameraRigTransform.position - headTransform.position; // Calculate the difference between the center of the virtual room & the player's head
        difference.y = 0; // Don't change the final position's y position, it should always be equal to that of the hit point

        cameraRigTransform.position = hitPoint + difference + new Vector3(0,0.25f,0); // Change the camera rig position to where the the teleport reticle was. Also add the difference so the new virtual room position is relative to the player position, allowing the player's new position to be exactly where they pointed. (see illustration)
    }
}
