﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpawnerBody : MonoBehaviour {

    private int LifePoint;
    private int contExplosionBoss;

    // Use this for initialization
    void Start () {
        LifePoint = 4;
        contExplosionBoss = 0;
    }

    // Update is called once per frame
    void Update () {

        if (contExplosionBoss == 50 && LifePoint==0)
        {
            var spawner = transform.parent.gameObject;
            Destroy(spawner.gameObject);
        }

        contExplosionBoss++;
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Ball"))
        {
            Destroy(collision.gameObject);
            DestroyBar();
        }
    }

    private void DestroyBar()
    {
        if(LifePoint != 0)
        {
            var LifeBar = GameObject.Find("Bar" + LifePoint.ToString());
            Destroy(LifeBar.gameObject);

            switch (LifePoint)
            {
                case 4:
                    // Destroy Motor Defense Boss

                    var armorObject = GameObject.Find("armor1");
                    var armorObject2 = GameObject.Find("armor2");

                    armorObject.GetComponent<Transform>().SetParent(null);
                    armorObject.GetComponent<BoxCollider>().enabled = true;
                    armorObject.AddComponent<Rigidbody>().freezeRotation=true;

                    armorObject2.GetComponent<Transform>().SetParent(null);
                    armorObject2.GetComponent<BoxCollider>().enabled = true;
                    armorObject2.AddComponent<Rigidbody>().freezeRotation = true;

                    break;
                case 3:
                    // Destroy Soplos and Ventilador  turbina boss
                    var soploObject = GameObject.Find("soplo3");
                    var soploObject2 = GameObject.Find("soplo4");
                    var ventiladorObject = GameObject.Find("vint11");
                    var ventiladorObject2 = GameObject.Find("vint12");

                    soploObject.GetComponent<Transform>().SetParent(null);
                    soploObject.AddComponent<Rigidbody>().freezeRotation = true;
                    soploObject.GetComponent<BoxCollider>().enabled = true;

                    soploObject2.GetComponent<Transform>().SetParent(null);
                    soploObject2.AddComponent<Rigidbody>().freezeRotation = true;
                    soploObject2.GetComponent<BoxCollider>().enabled = true;

                    ventiladorObject.GetComponent<Transform>().SetParent(null);
                    ventiladorObject.AddComponent<Rigidbody>().freezeRotation = true;
                    ventiladorObject.GetComponent<BoxCollider>().enabled = true;

                    ventiladorObject2.GetComponent<Transform>().SetParent(null);
                    ventiladorObject2.AddComponent<Rigidbody>().freezeRotation = true;
                    ventiladorObject2.GetComponent<BoxCollider>().enabled = true;

                    break;
                case 2:
                    //Destroy  head Turbina  Boss
                    var headObject = GameObject.Find("head1");
                    var headObject2 = GameObject.Find("head2");

                    headObject.GetComponent<Transform>().SetParent(null);
                    headObject.AddComponent<Rigidbody>().freezeRotation = true;
                    headObject.GetComponent<BoxCollider>().enabled = true;


                    headObject2.GetComponent<Transform>().SetParent(null);
                    headObject2.AddComponent<Rigidbody>().freezeRotation = true;
                    headObject2.GetComponent<BoxCollider>().enabled = true;

                    break;

                case 1:

                    var listObjects = transform.childCount;
                    //foreach(GameObject item in listObjects)
                    //{
                    //    item.AddComponent<Rigidbody>();
                    //}
                    for (int i=0;i< listObjects; i++)
                    {
                        var Object = transform.GetChild(i).gameObject;

                        Object.GetComponent<BoxCollider>().enabled = true;
                        Object.gameObject.AddComponent<Rigidbody>();
                    }
                    contExplosionBoss=0;
                    break;
                default:
                    break;
            }
            LifePoint -= 1;
        }

    }
}
