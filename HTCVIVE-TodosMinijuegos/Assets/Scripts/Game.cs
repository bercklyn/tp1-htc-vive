﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public GameObject Minijuego1;
    public GameObject Minijuego2;
    public GameObject Minijuego3;
    public GameObject Minijuego4;

    public GameObject GetMinijuego1()
    {
        return Minijuego1;
    }
    public GameObject GetMinijuego2()
    {
        return Minijuego2;
    }
    public GameObject GetMinijuego3()
    {
        return Minijuego3;
    }
    public GameObject GetMinijuego4()
    {
        return Minijuego4;
    }
}
