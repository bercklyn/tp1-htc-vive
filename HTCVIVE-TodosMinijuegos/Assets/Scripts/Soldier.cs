﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour {
    public GameObject parentSoldier;
    public int lifeBar;
    public int lifeBarMax;

	// Use this for initialization
	void Start () {
        lifeBar = lifeBarMax;
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Bullet"));
        {
            if (name.Contains("Head"))
            {
                Destroy(parentSoldier);
            }
            else
            {
                lifeBar--;
                if (lifeBar==0)
                {
                    Destroy(parentSoldier);
                }
            }
        }
    }
}
