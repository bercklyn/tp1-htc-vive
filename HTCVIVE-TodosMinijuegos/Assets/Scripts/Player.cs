﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour {

    
    public int HitPoints;
    public int EnemyKillWin;
    public GameObject NovaGP;
    public GameObject Panel;
    public GameObject lifeBar;
    public GameObject textNumEnemy;
    public GameObject Spawner;

    private int life;
    private int enemyKill;
    private void Start()
    {
        life = HitPoints;
        enemyKill = EnemyKillWin;
    }
    private void Awake()
    {
        textNumEnemy.GetComponent<TextMeshProUGUI>().text = EnemyKillWin.ToString() + "/"
                                                    + EnemyKillWin.ToString() + "\n"
                                                    + "numero de humanoides \n"
                                                    + "por eliminar";
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Enemy"))
        {
            Destroy(collision.gameObject);
            life--;
            lifeBar.transform.GetChild(life).gameObject.SetActive(false);

            if( life == 0)
            {
                GameOver(false);
            }

        }
    }
    private void FixedUpdate()
    {
        if (enemyKill == 0)
        {
            GameOver(true);
        }
    }
    public void UpdateEnemyKill()
    {
        enemyKill--;
        textNumEnemy.GetComponent<TextMeshProUGUI>().text = enemyKill.ToString() + "/"
                                                            + EnemyKillWin.ToString() + "\n"
                                                            + "numero de humanoides \n"
                                                            + "por eliminar";
                                                            
    }
    private void GameOver(bool win)
    {

        Spawner.GetComponent<SpawnerEnemy>().GameOver();
        NovaGP.SetActive(true);
        Panel.SetActive(true);
        var mensaje = NovaGP.transform.Find("Nova_Mensaje").Find("Mensaje").GetComponent<TextMeshProUGUI>();

        if (win)
        {
            mensaje.text = "ganaste";
        }
        else
        {
            mensaje.text = "perdiste";
        }
    }
    public void ResetPlayer()
    {

        Panel.SetActive(false);
        NovaGP.SetActive(false);

        life = HitPoints;
        for(int i=0;i<3; i++)
        {
            lifeBar.transform.GetChild(i).gameObject.SetActive(true);
        }
        enemyKill = EnemyKillWin;
        textNumEnemy.GetComponent<TextMeshProUGUI>().text = EnemyKillWin.ToString() + "/"
                                                    + EnemyKillWin.ToString() + "\n"
                                                    + "numero de humanoides \n"
                                                    + "por eliminar";
    }
}
