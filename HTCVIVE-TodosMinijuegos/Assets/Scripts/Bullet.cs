﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private int cont;
	// Use this for initialization
	void Start () {
        cont = 0;
	}

    private void FixedUpdate()
    {
        cont++;
        if(cont > 600)
        {
            Destroy(gameObject);
        }
    }
}
