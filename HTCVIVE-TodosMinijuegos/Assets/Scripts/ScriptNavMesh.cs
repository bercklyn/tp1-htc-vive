﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ScriptNavMesh : MonoBehaviour {


    public int lifeZombie;

    private GameObject positionPlayer;
    private NavMeshAgent nav;
    private GameObject SpawnerParent;

    // Use this for initialization
    void Start() {
        //positionPlayer = null;
    }

    // Update is called once per frame
    void Update() {
        if (positionPlayer != null)
        {
            nav.SetDestination(positionPlayer.transform.position);
        }
    }
    public void SetPositionPlayer(GameObject player)
    {
        nav = gameObject.GetComponent<NavMeshAgent>();
        positionPlayer = player;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.name.Contains("Bullet"))
    //    {
    //        lifeZombie--;
    //        Destroy(other.gameObject);
    //        // se destruye el zombie
    //        if (lifeZombie == 0)
    //        {
    //            //SpawnerParent.GetComponent<SpawnerZombie>().UpdateContZombie();
    //            Destroy(gameObject);
    //            // animacion   cuando se destruye el zombi

    //        }
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Bullet"))
        {
            lifeZombie--;
            Destroy(collision.gameObject);
            // se destruye el zombie
            if (lifeZombie == 0)
            {
                //SpawnerParent.GetComponent<SpawnerZombie>().UpdateContZombie();
                Destroy(gameObject);
                // animacion   cuando se destruye el zombi

            }
        }
    }

    public void SetVelocity(float velocity)
    {
        nav.speed = velocity;
    }
    public void SetSpawnerParent (GameObject spawner)
    {
        SpawnerParent = spawner;
    }
    private void OnDestroy()
    {
        if(SpawnerParent.GetComponent<SpawnerZombie>().isPlaying)
            SpawnerParent.GetComponent<SpawnerZombie>().UpdateContZombie();
    }
}
