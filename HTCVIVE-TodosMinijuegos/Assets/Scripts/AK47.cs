﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK47 : MonoBehaviour {


    public GameObject prefabBullet;
    public int force;
    public int Cartucho;
    public float Delay;

    private float contDelay;
    private int ContBullets;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        ContBullets = Cartucho;
        source = GetComponent<AudioSource>();
	}

    private void FixedUpdate()
    {
        contDelay--;
    }

    public void PlayShoot()
    {
        if (ContBullets > 0 && contDelay <=0)
        {
            var positionShoot = transform.Find("Shoot").transform;
            var bullet = Instantiate(prefabBullet, positionShoot.position, transform.rotation);

            // se activa el flash de del disparo
            var flash = transform.Find("MuzzleFlash").GetComponent<FlashShoot>();
            flash.PlayFlashShoot();

            // sonido de disparo
            source.Play();
            contDelay = Delay*60;
            bullet.name = "Bullet";
            var rigibody = bullet.GetComponent<Rigidbody>();
           // rigibody.useGravity = true;
            rigibody.velocity = positionShoot.forward * force;
            ContBullets--;
            //// se puede añadir la animacin de disparo miniexplosion del arma
        }

    }

    public  void ReloadMachineGun()
    {
        ContBullets = Cartucho;
    }
}
