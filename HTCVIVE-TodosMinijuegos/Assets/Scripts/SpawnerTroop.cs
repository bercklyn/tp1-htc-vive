﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpawnerTroop : MonoBehaviour {

    public int timeGameSecon;
    public GameObject Prefab;
    public GameObject NovaGP;
    public GameObject controlLeft;
    public GameObject controlRight;
    public GameObject textTime;
    public GameObject Panel;
    public GameObject LightCameraMain;



    private int soldiers;
    private int soldierfallens;
    private int Countdown;
    private GameObject Troop;

    private int count;
    private bool isPlaying;


    void Start () {
        soldierfallens = 0;
        soldiers = 6;
        Countdown = timeGameSecon;
        isPlaying = true;
    }
    private void Awake()
    {
        PlayGame();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (isPlaying)
        {
            var soldiers = GameObject.Find("Troop").transform.Find("Soldiers");
        
		    if(soldiers.transform.childCount == 0 )
            {
                GameOver(true);
            }

            if (count == 60)
            {
                textTime.GetComponent<TextMeshProUGUI>().text = "0" + (Countdown / 60).ToString()
                                                        + ":" + (Countdown % 60).ToString("D2");
                Countdown--;
                if (Countdown <0)
                {
                    GameOver(false);
                }
                count = 0;
                return;
            }
            count++;
        }

    }
    public void PlayGame()
    {
        Troop = Instantiate(Prefab, transform.position, transform.rotation);
        Troop.name = "Troop";
        soldierfallens = 0;
        soldiers = 6;
        Countdown = timeGameSecon;
        isPlaying = true;
        LightCameraMain.SetActive(false);
    }

    public void UpdateSoldierFallen()
    {
        soldierfallens++;
    }
    public void ResetGame()
    {
        NovaGP.SetActive(false);
        Panel.SetActive(false);
        controlRight.transform.Find("SniperDSR50").gameObject.SetActive(true);
        controlLeft.GetComponent<ControlJuego4>().isEndGame = false;
        controlRight.GetComponent<ControlJuego4>().isEndGame = false;
        LightCameraMain.SetActive(false);

        Destroy(Troop);
        PlayGame();
    }

    private void GameOver(bool win)
    {
        isPlaying = false;
        NovaGP.SetActive(true);
        Panel.SetActive(true);
        controlRight.transform.Find("SniperDSR50").gameObject.SetActive(false);
        controlLeft.GetComponent<ControlJuego4>().isEndGame = true;
        controlRight.GetComponent<ControlJuego4>().isEndGame = true;
        LightCameraMain.SetActive(true);



        var mensaje = NovaGP.transform.Find("Nova_Mensaje").Find("Mensaje").GetComponent<TextMeshProUGUI>();

        if (win)
        {
            mensaje.text = "ganaste";
        }
        else
        {
            mensaje.text = "perdiste";
        }
    }

}
