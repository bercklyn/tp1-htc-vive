﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class StartBoss : MonoBehaviour {

    public GameObject prefabBoss;
    public GameObject positionPlayer;
    public int timeGame;
    public GameObject canvasCamera;
    public GameObject textCount;
    public GameObject NovaGP;
    public GameObject NovaPlayer;
    public GameObject Panel;
    public GameObject controlLeft;
    public GameObject controlRight;

    private int countGame;
    private int count;
    private GameObject boss;
    private TextMeshProUGUI mensaje;
    private  bool isPlaying;

    // Use this for initialization
    void Start () {
        StartMiniGame();
        countGame = timeGame;
        count = 0;
        isPlaying = true;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (isPlaying)
        {
            if (count == 60)
            {
                textCount.GetComponent<TextMeshProUGUI>().text= "0"+ (countGame/60).ToString() 
                                                                +":"+ (countGame % 60).ToString("D2");
                countGame--;
                if (countGame == 0)
                {
                    GameOver(false);
                    Destroy(boss);
                }
                count = 0;
                return;
            }

            count++;
        }

    }

    public void StartMiniGame()
    {
        boss = Instantiate(prefabBoss, transform);
        boss.GetComponent<Spawner>().PositionPlayer = positionPlayer;
    } 
    public void GameOver(bool win)
    {
        if (win && countGame <= 0)
            return;

        isPlaying = false;
        Panel.SetActive(true);
        NovaPlayer.SetActive(false);
        NovaGP.SetActive(true);
        controlLeft.GetComponent<ControlJuego1>().isEndGame = true;
        controlRight.GetComponent<ControlJuego1>().isEndGame = true;

        controlLeft.GetComponent<ControlJuego1>().ReleaseObject();
        controlRight.GetComponent<ControlJuego1>().ReleaseObject();

        mensaje = NovaGP.transform.Find("Nova_Mensaje").Find("Mensaje").GetComponent<TextMeshProUGUI>();

        if (win)
        {
            mensaje.text = "ganaste";
        }
        else
        {
            mensaje.text = "perdiste";
        }
    }
    public void ResetGame()
    {
        Panel.SetActive(false);
        NovaPlayer.SetActive(true);
        NovaGP.SetActive(false);
        StartMiniGame();      
        countGame = timeGame;
        count = 0;
        isPlaying = true;
    }

}
