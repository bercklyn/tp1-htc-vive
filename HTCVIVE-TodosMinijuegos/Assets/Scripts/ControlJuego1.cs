﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego1 : MonoBehaviour
{
    public Transform cameraRigTransform;
    public Transform headTransform; // The camera rig's head
    public LayerMask teleportMask; // Mask to filter out areas where teleports are allowed
    public GameObject SpawnerBoss;
    public GameObject laserPrefab; // The laser prefab
    public int force;

    public GameObject ControlLeft;
    public GameObject ControlRight;
    public GameObject CanvasCamera;
    public bool isPaused;
    public bool isEndGame;

    private GameObject laser; // A reference to the spawned laser
    private Transform laserTransform; // The transform component of the laser for ease of use
    public GameObject teleportReticlePrefab; // Stores a reference to the teleport reticle prefab.

    private Vector3 hitPoint; // Point where the raycast hits
    private bool shouldTeleport; // True if there's a valid teleport target
    private GameObject raycastObject;


    private SteamVR_TrackedObject trackedObj;
    private GameObject collidingObject;
    private GameObject objectInHand;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();

    }


    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }

    public void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
    }

    public void OnTriggerExit(Collider other)
    {
        if (!collidingObject)
        {
            return;
        }
        collidingObject = null;
    }

    private void SetCollidingObject(Collider col)
    {
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }

        collidingObject = col.gameObject;
    }

    void Update()
    {
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if(!isEndGame)
                PauseGame();
        }
        if (isPaused || isEndGame)
        {
            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
            {
                RaycastHit hit;

                // Send out a raycast from the controller
                if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
                {
                    hitPoint = hit.point;
                    raycastObject = hit.collider.gameObject;

                    ShowLaser(hit);
                    shouldTeleport = true;
                }
            }
            else // Touchpad not held down, hide laser & teleport reticle
            {
                laser.SetActive(false);
                //reticle.SetActive(false);
            }
            // Is the touchpad held down?


            // Touchpad released this frame & valid teleport position found
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
            {
                //logica de Teleport segun juego
                switch (raycastObject.name)
                {
                    case "Nova_Salir": //va al inicio    
                        Time.timeScale = 1;
                        SceneManager.LoadScene("Game");
                        break;
                    case "Nova_Again": // vuelves a jugar
                        ControlLeft.GetComponent<ControlJuego1>().isEndGame = false;
                        ControlRight.GetComponent<ControlJuego1>().isEndGame = false;
                        SpawnerBoss.GetComponent<StartBoss>().ResetGame();
                        break;
                    case "Nova_Control":
                        var panelinst = CanvasCamera.transform.Find("Nova_Pause").Find("PanelInstruccion");
                        panelinst.transform.Find("UsoMando").gameObject.SetActive(true);
                        panelinst.transform.Find("Informacion").gameObject.SetActive(false);
                        break;
                    case "Nova_Informacion":
                        var panelinst2 = CanvasCamera.transform.Find("Nova_Pause").Find("PanelInstruccion");
                        panelinst2.transform.Find("UsoMando").gameObject.SetActive(false);
                        panelinst2.transform.Find("Informacion").gameObject.SetActive(true);
                        break;
                    case "Salir":
                        PauseGame();
                        break;
                }
            }

        }
        else
        {

            if (Controller.GetHairTriggerDown())
                {
                    if (collidingObject)
                    {
                        GrabObject();
                    }
                }

                if (Controller.GetHairTriggerUp())
                {
                    if (objectInHand)
                    {
                        ReleaseObject();
                    }
                }
        }
  
    }


    private void GrabObject()
    {
        objectInHand = collidingObject;
        collidingObject = null;
        var joint = AddFixedJoint();
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();

    }

    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    public void ReleaseObject()
    {
        if (GetComponent<FixedJoint>())
        {
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            objectInHand.GetComponent<Rigidbody>().velocity = Controller.velocity * force;
            objectInHand.GetComponent<Rigidbody>().angularVelocity = Controller.angularVelocity * force;
            objectInHand = null;

        }

        //  objectInHand = null;
    }

 


    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        //reticle = Instantiate(teleportReticlePrefab);
        //teleportReticleTransform = reticle.transform;
        isPaused = false;
        isEndGame = false;

    }

    private void ShowLaser(RaycastHit hit)
    {
        laser.SetActive(true); //Show the laser
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f); // Move laser to the middle between the controller and the position the raycast hit
        laserTransform.LookAt(hitPoint); // Rotate laser facing the hit point
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance); // Scale laser so it fits exactly between the controller & the hit point
    }

    private void ShowPause(bool show)
    {
        CanvasCamera.transform.Find("Nova_Pause").gameObject.SetActive(show);
        CanvasCamera.transform.Find("Panel").gameObject.SetActive(show);

    }

    private void PauseGame()
    {
        if (!isPaused)
        {
            Time.timeScale = 0;
            isPaused = true;
            ShowPause(true);

        }
        else
        {
            Time.timeScale = 1;
            isPaused = false;
            ShowPause(false);

        }
    }

}
