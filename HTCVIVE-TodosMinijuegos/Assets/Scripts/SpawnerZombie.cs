﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.SceneManagement;
//using System;

public class SpawnerZombie : MonoBehaviour {

    public GameObject zombiePrefab;
    public GameObject positionPlayer;
    public GameObject positionSpawn1;
    public GameObject positionSpawn2;
    public GameObject controlLeft;
    public GameObject controlRight;


    public GameObject NovaGP;
    public bool isPlaying;

    private int cont;
    private int contZombie;
    private int Wave;
    private bool contWave;
    private int contZero;
    private int contDescont;
    private GameObject Title;
    private GameObject Count;
    private GameObject canvasCamera;
    private GameObject Panel;
    private TextMeshProUGUI mensaje;

    // Use this for initialization
    void Awake() {
        LoadVariables();
        PlayGame();
    }

    private void LoadVariables()
    {
        contZombie = 0;
        Wave = 0;
        canvasCamera= GameObject.Find("CanvasCamera").transform.Find("WavePanel").gameObject;
        Panel = GameObject.Find("CanvasCamera").transform.Find("Panel").gameObject;
        Title = canvasCamera.transform.Find("TextTitle").gameObject;
        Count = canvasCamera.transform.Find("TextCount").gameObject;
        contZero = 7;
        isPlaying = false;
        contDescont = contZero;
    }

    private void FixedUpdate()
    {
        if (isPlaying)
        {
            if (contZombie <= 0)
            {
                StartCountWave();
            }
        }
        if (contWave)
        {
            if(cont >= 60)
            {
                cont = 0;
                contDescont--;
                Count.GetComponent<TextMeshProUGUI>().text = "00:0"+ contDescont.ToString();
                if(contDescont <= 0)
                {

                    contWave = false;
                    Title.GetComponent<TextMeshProUGUI>().text = string.Empty;
                    Count.GetComponent<TextMeshProUGUI>().text = string.Empty;
                    canvasCamera.SetActive(false);
                    Panel.SetActive(false);

                    switch (Wave)
                    {
                        case 0:
                            Wave1();
                            break;
                        case 1:
                            Wave2();
                            break;
                        case 2:
                            Wave3();
                            break;
                    }
                    isPlaying = true;
                }
            }
            cont++;
        }

    }

    private void Wave1()
    {
        var position = positionSpawn1.transform;        
        for( int z=0;z<6;z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(2, 4);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }

        position = positionSpawn2.transform;
        for (int z = 0; z < 6; z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(2, 4);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }

        Wave = 1;
    }

    private void Wave2()
    {
        var position = positionSpawn1.transform;
        for (int z = 0; z < 8; z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(3, 5);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }

         position = positionSpawn2.transform;
        for (int z = 0; z < 8; z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(3, 5);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }
         Wave = 2;

    }

    private void Wave3()
    {
        var position = positionSpawn1.transform;
        for (int z = 0; z < 8; z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(3, 5);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }

        position = positionSpawn2.transform;
        for (int z = 0; z < 8; z++)
        {
            var randomx = Random.Range(-8, 8);
            var randomz = Random.Range(-8, 8);
            var velocity = Random.Range(3, 5);
            var zombie = Instantiate(zombiePrefab, position.position + new Vector3(randomx, 0, randomz), transform.rotation);
            var script = zombie.GetComponent<ScriptNavMesh>();
            script.SetPositionPlayer(positionPlayer);
            script.SetVelocity(velocity);
            script.SetSpawnerParent(gameObject);
            contZombie++;
        }
        Wave = 3;

    }

    public void UpdateContZombie()
    {
        contZombie--;
    }

    public void ResetGame()
    {
        controlRight.transform.Find("AK47").gameObject.SetActive(true);
        Panel.SetActive(false);
        NovaGP.SetActive(false);
        isPlaying = false;
        Wave = 0;
        contZombie = 0;
        contWave = false;
        PlayGame();

    }

    public void GameOver(int status)
    {
        Panel.SetActive(true);
        NovaGP.SetActive(true);
        controlLeft.GetComponent<ControlJuego3>().isEndGame = true;
        controlRight.GetComponent<ControlJuego3>().isEndGame = true;
        //controlLeft.transform.Find("Model").gameObject.SetActive(show);
        //ControlRight.transform.Find("Model").gameObject.SetActive(show);
        controlRight.transform.Find("AK47").gameObject.SetActive(false);

        mensaje = NovaGP.transform.Find("Nova_Mensaje").Find("Mensaje").GetComponent<TextMeshProUGUI>();
        isPlaying = false;

        if (status == 0)
        {
            var zombis = GameObject.FindGameObjectsWithTag("zombie");

            for (int i = 0; i < zombis.Length; i++)
            {
                Destroy(zombis[i]);
            }

            mensaje.text = "perdiste";
        }
        else
        {
            mensaje.text = "ganaste";
        }
    }

    public void PlayGame()
    {
        StartCountWave();
    }

    private void StartCountWave()
    {
        if (Wave <= 2)
        {
            canvasCamera.SetActive(true);
            cont = 0;
            contDescont = contZero;
            contWave = true;
            isPlaying = false;
            Title.GetComponent<TextMeshProUGUI>().text = "OLEADA  N°  " + (Wave+1).ToString()  + "\n " +
                                                         "COMIENZA EN \n \n\n" +
                                                         " !preparate¡";
            Count.GetComponent<TextMeshProUGUI>().text = "00:0"+ contZero.ToString();
        }
        if (Wave == 3)
        {
            GameOver(1);
        }
    }
}
