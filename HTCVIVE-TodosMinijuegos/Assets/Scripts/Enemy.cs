﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public GameObject prefabExplosion;

    private GameObject positionPlayer;
    private NavMeshAgent nav;
    private Material enemySlow;
    private Material enemyFast;
    private Material enemyMediun;
    private int lifeBar;
    private int random;
        // Use this for initialization
	void Start () {
        lifeBar = 3;
        random = Random.Range(2, 5);
        //nav = gameObject.GenavtComponent<NavMeshAgent>(); 
    }
	
	// Update is called once per frame
	void Update () {

        if(positionPlayer != null)
        {
            nav.SetDestination(positionPlayer.transform.position);
        }
        if(random == 0)
        {
            attackToPlayer();
        }

        random--;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Arrow"))
        {
            Destroy(collision.gameObject);
            // lifeBar--;
            UpdateLifeBar();
            if (lifeBar == 0)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
                Instantiate(prefabExplosion, transform.position, transform.rotation);
                var camareTrasform = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                camareTrasform.Find("PositionPlayer").GetComponent<Player>().UpdateEnemyKill();

            }

        }
    }

    public void SetTypeEnemy( int type, Material enemyMaterial, GameObject _positionPlayer)
    {
        ///type 1 enemyslow 
        ///type 2 enemy fast

        nav = gameObject.GetComponent<NavMeshAgent>(); 

        switch (type)
        {
            case 1:
                lifeBar = 2;
                nav.speed = 3;

                break;
            case 2:
                lifeBar = 1;
                nav.speed = 5;
                transform.Find("default").GetComponent<MeshRenderer>().material = enemyMaterial;// = enemyMaterial.mainTexture;

                break;
            default:
                break;
        }
        positionPlayer = _positionPlayer;
    }

    public void SetAtributeEnemy(Material enemyMatSlow, Material enemyMatMediun, Material enemyMatFast, GameObject _positionPlayer)
    {
        nav = gameObject.GetComponent<NavMeshAgent>();

        nav.speed = 4;
        enemyFast = enemyMatFast;
        enemyMediun = enemyMatMediun;
        enemySlow = enemyMatSlow;
        positionPlayer = _positionPlayer;
        transform.Find("default").GetComponent<MeshRenderer>().material = enemySlow;

    }
    public void UpdateLifeBar()
    {
        var playerGroup = gameObject.transform.GetComponentsInChildren<Animator>();
        switch (lifeBar)
        {
            case 3:
                nav.speed = 3;
                //Color mediun = new Color();
                // ColorUtility.TryParseHtmlString("#FF9622FF", out mediun);
                // transform.GetComponent<MeshRenderer>().material.SetColor("_TintColor", mediun);
                
                for (int i = 0; i < playerGroup.Length; i++)
                {
                    playerGroup[i].Play("Basic_Run_02");
                }
                ///transform.Find("default").GetComponent<MeshRenderer>().material = enemyMediun;
                break;
            case 2:
                nav.speed = 2;
                 playerGroup = gameObject.transform.GetComponentsInChildren<Animator>();
                for (int i = 0; i < playerGroup.Length; i++)
                {
                    playerGroup[i].Play("Etc_Walk_Zombi_01");
                }
                // transform.Find("default").GetComponent<MeshRenderer>().material = enemyFast; 
                break;
            default: break;
        }
        lifeBar--;
    }
    private void attackToPlayer()
    {
        var random = Random.Range(1, 3) * 60;

        var direction = positionPlayer.transform.position - transform.position;

    }
}
