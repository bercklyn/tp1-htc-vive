﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplosion : MonoBehaviour {

    private int cont;
	// Use this for initialization
	void Start () {
        cont = 0;
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        cont++;

        if(cont == 150)
        {
            Destroy(gameObject);
        }

	}
}
