﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashShoot : MonoBehaviour {


    private int cont;

	// Use this for initialization
	void Start () {
        cont = 0;
	}
    private void FixedUpdate()
    { 
        if(cont < 0)
        {
            gameObject.GetComponent<ParticleRenderer>().enabled = false;
        }
        cont--;
    }

    public void PlayFlashShoot()
    {
        cont = 5;
        gameObject.GetComponent<ParticleRenderer>().enabled = true;
    }
}
