﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScriptCountToHome : MonoBehaviour {


    private int numMiniJuego;


    private int cont;
    private int contEndGame;
    private GameObject controlLeft;
    private GameObject controlRight;
    private Transform camareTrasform;
    private Transform positionPlayer;
    private Transform lightCamare;

    // Use this for initialization
    void Start () {
        numMiniJuego = -1;
        cont = 0;
        contEndGame = 4;
	}
	
    private void FixedUpdate()
    {
        if (numMiniJuego > 0)
        {
            if (cont == 60)
            {
                contEndGame--;
                gameObject.GetComponent<TextMeshPro>().text = contEndGame.ToString();
                cont = 0;
                if (contEndGame == 0)
                {
                    ClearGame();    
                    SetCamareToHome();
                }
            }

            cont++;
        }
    }
    private void ClearGame()
    {
        transform.parent.Find("TextTitle").GetComponent<TextMeshPro>().text = "";
        gameObject.GetComponent<TextMeshPro>().text = "";

        switch (numMiniJuego)
        {
            case 2:
                 controlLeft = GameObject.Find("Controller (left)");
                 controlRight = GameObject.Find("Controller (right)");
                 camareTrasform = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                 positionPlayer = camareTrasform.Find("PositionPlayer");

                // Desactivamos el script del jugador y su capsule collider
                positionPlayer.GetComponent<Player>().enabled = false;
                positionPlayer.GetComponent<CapsuleCollider>().enabled = false;


                // Habilitamos el box collider 
                controlLeft.GetComponent<BoxCollider>().enabled = true;
                controlRight.GetComponent<BoxCollider>().enabled = true;

                // añadimos  el rigibody
                controlLeft.AddComponent<Rigidbody>().isKinematic = true;
                controlRight.AddComponent<Rigidbody>().isKinematic = true;

                // volvemos a activar   los models  de los controles
                controlLeft.transform.FindChild("Model").gameObject.SetActive(true);
                controlRight.transform.FindChild("Model").gameObject.SetActive(true);

                // desactiva el arco del control y spawnerenemy
                controlLeft.transform.FindChild("Arco").gameObject.SetActive(false);
                Destroy(controlRight.transform.Find("Arrow(Clone)").gameObject);

                // desactivamos el script de control right
                controlRight.GetComponent<ArrowManager>().enabled = false;
                positionPlayer.GetComponent<Player>().ResetPlayer();
                positionPlayer.GetComponent<Player>().enabled = false;
                Destroy(positionPlayer.GetComponent<Rigidbody>());
                break;
            case 3:
                controlLeft = GameObject.Find("Controller (left)");
                controlRight = GameObject.Find("Controller (right)");
                camareTrasform = GameObject.Find("[CameraRig]").GetComponent<Transform>();
                positionPlayer = camareTrasform.Find("PositionPlayer");
                lightCamare = camareTrasform.Find("LightCamare");

                lightCamare.gameObject.SetActive(true);

                // activamos  los models  de los controles
                controlLeft.transform.Find("Model").gameObject.SetActive(true);
                controlRight.transform.Find("Model").gameObject.SetActive(true);

                //activamos los scripts del mando
                controlLeft.GetComponent<ControllerGrabObject>().enabled = true;
                controlRight.GetComponent<ControllerGrabObject>().enabled = true;


                // desactivamos el script de la metralladora
                controlLeft.GetComponent<ControlJuego3>().enabled = false;
                controlRight.GetComponent<ControlJuego3>().enabled = false;


                // desactivamos  el model de la metralladora
                controlRight.transform.Find("AK47").gameObject.SetActive(false);

                break;
        }

        numMiniJuego = -1;
        contEndGame = 4;
        cont = 0;
    }

    private void SetCamareToHome()
    {
        var camare = GameObject.Find("[CameraRig]").transform;
        var positionHome = GameObject.Find("PositionHome").transform;
       // camare.Find("PositionPlayer").GetComponent<CapsuleCollider>().enabled = false;
        camare.SetPositionAndRotation(positionHome.position, camare.rotation);

    }
    public void StartCount(int _numMiniJuego)
    {
        gameObject.GetComponent<TextMeshPro>().text = contEndGame.ToString();
        numMiniJuego = _numMiniJuego;
    }
}
