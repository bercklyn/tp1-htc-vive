﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Spawner : MonoBehaviour {


    public GameObject prefabBall;
    public GameObject PositionPlayer;
    
    private float time;
    private float spawnTime;
    private float MovY;
    private float PosInitY;

    // Use this for initialization
	void Start () {

        time =4;
        spawnTime=12;
        MovY=0.04f;

        PosInitY = this.gameObject.transform.position.y;
    }

    // Update is called once per frame

     void FixedUpdate()
    {
        time += Time.deltaTime;
        
        if(time >= spawnTime)
        {
            NewBall();
            SetSpawnTimeRandom();
        }

        var transform = gameObject.GetComponent<Transform>();

        if (transform.position.y - PosInitY < 0 || transform.position.y - PosInitY > 7)
            MovY *= -1;

        transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + MovY, transform.position.z),transform.rotation);

     
    }

    void NewBall()
    {
        time = 0;
        // calcular vector direccion hacia el player
        var objectSpawner = this.gameObject.GetComponent<Transform>();
        var objectPlayer = GameObject.Find("PositionPlayer").GetComponent<Transform>();

        var direction = objectPlayer.position - objectSpawner.position;

        /// fin calculo de vector

        var ball =Instantiate(prefabBall, transform.position + new Vector3(2,0,0), transform.rotation);
        var rb = ball.GetComponent<Rigidbody>();
        var random = Random.Range(-0.20f,0.60f);
        //rb.AddForce(new Vector3(20f,5.8f ,random),ForceMode.Impulse);

        /// Ajuste de  vector direccion con variable random
        /// 
        rb.AddForce(new Vector3(15f,direction.y,random),ForceMode.Impulse);

    }

    void SetSpawnTimeRandom()
    {
        spawnTime = Random.Range(2, 3);
    }

    private void OnDestroy()
    {
        transform.parent.GetComponent<StartBoss>().GameOver(true);
    }

}
