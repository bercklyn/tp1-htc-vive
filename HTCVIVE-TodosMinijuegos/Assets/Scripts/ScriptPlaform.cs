﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlaform : MonoBehaviour {

    public GameObject prefabExplosion;

    private void Start()
    {
        prefabExplosion.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.name.Contains("Ball") && !collision.gameObject.name.Contains("Escudo"))
        {
            var transform = collision.gameObject.GetComponent<Transform>();
            Destroy(collision.gameObject);
            Instantiate(prefabExplosion, transform.position, transform.rotation);
            prefabExplosion.SetActive(true);

        }
    }
}
